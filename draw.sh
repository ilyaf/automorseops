#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This is skeleton template for new .sh files
set -ufo pipefail
export SHELLOPTS
IFS=$'\t\n'

URL='https://gitlab.com/yakshaving.art/dockerfiles/_template/container_registry'
MESSAGES=("sos" "halp" "gitlab" "oh noes" "auto" "morse" "oops")
API='http://www.morsecode-api.de/encode'
PS=4	# amplitude
BASE=60	# resolution

function _dot() {
	for i in $( seq 1 "${BASE}" ); do
		seq 1 "${PS}" | xargs -r -n1 -P "${PS}" curl -sSI "${URL}" >/dev/null 2>&1
		sleep 1
	done
	sleep "${BASE}"
}

function _dash() {
	# like dot, but 2x wide
	# shellcheck disable=SC2034
	for i in $( seq 1 "$((3 * BASE))" ); do
		seq 1 "${PS}" | xargs -r -n1 -P "${PS}" curl -sSI "${URL}" >/dev/null 2>&1
		sleep 1
	done
	sleep "${BASE}"
}

TEXT="${MESSAGES[${RANDOM} % ${#MESSAGES[@]}]}"

CODED="$(curl -qsSLG --data-urlencode "string=${TEXT}" "${API}" | jq -r '.morsecode')"

# runners live 1hour, so make sure we can draw it
dots="$(echo "${CODED}" | tr -cd '.' | wc -c)"
dashes="$(echo "${CODED}" | tr -cd '-' | wc -c)"
pauses="$(echo "${CODED}" | tr -cd ' ' | wc -c)"
runtime="$(( BASE * $(( 2*dots + 4*dashes + 2*pauses)) ))"

echo "Text '${TEXT}' will take ${runtime} seconds to draw"

# leave some room
if [[ 3300 -lt "${runtime}" ]]; then
	exit 1
fi

sleep "$(( 60 - $(date '+%S') ))"

while read -r -n1 ; do
	case "x${REPLY}x" in
		"x.x")
			echo "drawing dot"
			_dot
			;;
		"x-x")
			echo "drawing dash"
			_dash
			;;
		"x x")
			echo "sleeping"
			sleep "$((2 * BASE))"
			;;
	esac
done <<< "${CODED}"
